const cats = require("./cats.json");
const express = require('express');

const app = express();
const port = process.env.PORT || 5000;

app.get('/', (req, res) => res.send('Hello Devops Enabler & Co !'));

app.get('/cats', (req, res) => res.json({ cats }))

app.listen(port, () => console.log(`Example app listening on port ${port}!`));

